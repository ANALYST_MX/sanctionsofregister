GXX := g++
GXXFLAG := -std=c++14 -Wall -g
MAIN := main
CXX := .cxx
HXX := .hxx
OBJ := .o
OBJ_LIST := $(MAIN)$(OBJ) case/Register$(OBJ) util/XMLFile$(OBJ) util/XMLString$(OBJ) common/error/$(MAIN)$(OBJ) common/error/ReadException$(OBJ) entity/Money$(OBJ) entity/Sanction$(OBJ) entity/History$(OBJ)
LIB_LIST := -lm -lxml2
INCLUDE_DIR_LIST := -I. -I/usr/include/libxml2
all: $(MAIN)
$(MAIN): $(OBJ_LIST)
	$(GXX) $(GXXFLAG) $(INCLUDE_DIR_LIST) $(OBJ_LIST) -o $(MAIN) $(LIB_LIST)
$(MAIN)$(OBJ): $(MAIN)$(CXX) $(MAIN)$(HXX)
	$(GXX) $(GXXFLAG) $(INCLUDE_DIR_LIST) -c $(MAIN)$(CXX) -o $(MAIN)$(OBJ)
case/Register$(OBJ): case/Register$(CXX) case/Register$(HXX)
	$(GXX) $(GXXFLAG) $(INCLUDE_DIR_LIST) -c case/Register$(CXX) -o case/Register$(OBJ)
util/XMLFile$(OBJ): util/XMLFile$(CXX) util/XMLFile$(HXX)
	$(GXX) $(GXXFLAG) $(INCLUDE_DIR_LIST) -c util/XMLFile$(CXX) -o util/XMLFile$(OBJ)
util/XMLString$(OBJ): util/XMLString$(CXX) util/XMLString$(HXX)
	$(GXX) $(GXXFLAG) $(INCLUDE_DIR_LIST) -c util/XMLString$(CXX) -o util/XMLString$(OBJ)
common/error/$(MAIN)$(OBJ): common/error/$(MAIN)$(CXX) common/error/$(MAIN)$(HXX)
	$(GXX) $(GXXFLAG) $(INCLUDE_DIR_LIST) -c common/error/$(MAIN)$(CXX) -o common/error/$(MAIN)$(OBJ)
common/error/ReadException$(OBJ): common/error/ReadException$(CXX) common/error/ReadException$(HXX)
	$(GXX) $(GXXFLAG) $(INCLUDE_DIR_LIST) -c common/error/ReadException$(CXX) -o common/error/ReadException$(OBJ)
entity/Money$(OBJ): entity/Money$(CXX) entity/Money$(HXX)
	$(GXX) $(GXXFLAG) $(INCLUDE_DIR_LIST) -c entity/Money$(CXX) -o entity/Money$(OBJ)
entity/Sanction$(OBJ): entity/Sanction$(CXX) entity/Sanction$(HXX)
	$(GXX) $(GXXFLAG) $(INCLUDE_DIR_LIST) -c entity/Sanction$(CXX) -o entity/Sanction$(OBJ)
entity/History$(OBJ): entity/History$(CXX) entity/History$(HXX)
	$(GXX) $(GXXFLAG) $(INCLUDE_DIR_LIST) -c entity/History$(CXX) -o entity/History$(OBJ)
.phony: clean
clean:
	rm -rf $(MAIN) $(OBJ_LIST)
