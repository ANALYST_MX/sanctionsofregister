#pragma once

#include <string>
#include <exception>

class ReadException : public std::exception
{
public:
  ReadException(std::string) throw();
  virtual const char* what() const throw();
  virtual ~ReadException() throw();
private:
  std::string msg;
};
