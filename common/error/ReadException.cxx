#include "ReadException.hxx"

ReadException::ReadException(std::string newMsg) throw()
  : msg(newMsg)
{
}

const char* ReadException::what() const throw()
{
  return msg.c_str();
}

ReadException::~ReadException() throw()
{
}
