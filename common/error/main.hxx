#pragma once

#include <iostream>

enum ERROR {
  ERROR_USAGE_MAIN = 1,
  READ_FILE_ERROR,
};

void printError(int e);
