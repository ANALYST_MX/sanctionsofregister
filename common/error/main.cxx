#include "main.hxx"

void printError(int e)
{
  switch(e)
    {
    case ERROR_USAGE_MAIN:
      std::cerr << "error: ./main <register_file_name>" << std::endl;
      break;
    case READ_FILE_ERROR:
      std::cerr << "error: open register file" << std::endl;
      break;
    }
}
