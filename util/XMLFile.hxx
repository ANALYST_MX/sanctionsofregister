#pragma once

#include <libxml/parser.h>
#include <libxml/tree.h>

#include <string>

#include "common/error/main.hxx"
#include "common/error/ReadException.hxx"

class XMLFile
{
public:
  XMLFile();
  XMLFile(std::string);
  ~XMLFile();
  void setPath(std::string);
  std::string getPath();
  xmlNode *getRootElement();
protected:
  void init();
private:
  std::string path;
  xmlDoc *doc;
};
