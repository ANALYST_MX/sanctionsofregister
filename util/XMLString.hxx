#pragma once

#include <libxml/xmlstring.h>

#include <string>
#include <algorithm>
#include <iterator>

class XMLString
{
public:
  XMLString(const xmlChar * const);
  virtual ~XMLString();
  const std::string &toString() const;
  static std::string &rtrim(std::string &);
  static std::string &ltrim(std::string &);
  static std::string &trim(std::string &);
private:
  std::string strValue;
};
