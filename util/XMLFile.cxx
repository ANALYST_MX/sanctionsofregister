#include "XMLFile.hxx"

XMLFile::XMLFile()
{
  doc = nullptr;

  init();
}

XMLFile::XMLFile(std::string newPath)
{
  doc = nullptr;

  init();
  setPath(newPath);
}

XMLFile::~XMLFile()
{
  if (doc)
    {
      xmlFreeDoc(doc);
      xmlCleanupParser();
    }
}

void XMLFile::init()
{
  LIBXML_TEST_VERSION
}

void XMLFile::setPath(std::string newPath)
{
  path = newPath;
  if (nullptr == (doc = xmlReadFile(path.c_str(), NULL, 0)))
    {
      printError(READ_FILE_ERROR);
      throw ReadException("File not found");
    }
}

xmlNode *XMLFile::getRootElement()
{
  return xmlDocGetRootElement(doc);
}

std::string XMLFile::getPath()
{
  return path;
}
