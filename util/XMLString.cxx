#include "XMLString.hxx"

XMLString::XMLString(const xmlChar * const xmlCharValue)
{
  if (xmlCharValue)
    {
      strValue = (char *) xmlCharValue;
    }
  else
    {
      strValue = "";
    }
}

XMLString::~XMLString()
{
}

const std::string &XMLString::toString() const
{
  return strValue;
}

std::string &XMLString::rtrim(std::string &str)
{
  if (!isspace(str.front()))
    {
      return str;
    }
  str.erase(begin(str), find_if(begin(str), end(str), [](auto i) { return isspace(i); }));
  
  return str;
}

std::string &XMLString::ltrim(std::string &str)
{
  if (!isspace(str.back()))
    {
      return str;
    }
  str.erase(
	    find_if(str.rbegin(), str.rend(), [](auto i) { return isspace(i); })
            .base(),
	    end(str));
  
  return str;
}

std::string &XMLString::trim(std::string &str)
{
  return rtrim(ltrim(str));
}
