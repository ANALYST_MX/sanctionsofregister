#pragma once

#include <string>

#include "util/XMLFile.hxx"
#include "util/XMLString.hxx"
#include "entity/History.hxx"

class Register
{
public:
  Register(std::string);
  void outputSanctions();
  ~Register();
protected:
  void outputElement(xmlNode *);
private:
  XMLFile *file;
  History *h;
  Sanction *s;
};
