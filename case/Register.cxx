#include "Register.hxx"

Register::Register(std::string path)
{
  file = new XMLFile(path);
}

Register::~Register()
{
  delete file;
  if (h)
    {
      delete h;
    }
}

void Register::outputSanctions()
{
  outputElement(file->getRootElement());
}

void Register::outputElement(xmlNode *node)
{
  for (; node; node = node->next)
    {
      if (node->type == XML_ELEMENT_NODE)
	{
	  if (xmlStrEqual(node->name, BAD_CAST("ZAP")))
	    {
	      if (h)
		{
		  if (!h->getSanctions().empty())
		    {
		      std::cout << h->toString();
		    }
		  delete h;
		}
	      h = new History;
	    }
	  else if (xmlStrEqual(node->name, BAD_CAST("NHISTORY")))
	    {
	      h->setHistoryNumber(XMLString(xmlNodeGetContent(node)).toString());
	    }
	  else if (xmlStrEqual(node->name, BAD_CAST("LPU_DEP")))
	    {
	      h->setDepartment(XMLString(xmlNodeGetContent(node)).toString());
	    }
	  else if (xmlStrEqual(node->name, BAD_CAST("SANK")))
	    {
	      s = new Sanction;
	      h->addSanction(s);
	    }
	  else if (xmlStrEqual(node->name, BAD_CAST("S_SUM")))
	    {
	      s->setAmount(XMLString(xmlNodeGetContent(node)).toString());
	    }
	  else if (xmlStrEqual(node->name, BAD_CAST("S_OSN")))
	    {
	      s->setCode(std::stoi(XMLString(xmlNodeGetContent(node)).toString()));
	    }
	  else if (xmlStrEqual(node->name, BAD_CAST("S_COM")))
	    {
	      s->setComment(XMLString(xmlNodeGetContent(node)).toString());
	    }
	}
      outputElement(node->children);
    }
}
