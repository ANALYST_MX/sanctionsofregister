#include "main.hxx"

int main(int argc, char *argv[])
{
  if (2 != argc)
    {
      printError(ERROR_USAGE_MAIN);
      return(EXIT_FAILURE);
    }

  Register reg(argv[1]);
  reg.outputSanctions();
  
  return EXIT_SUCCESS;
}
