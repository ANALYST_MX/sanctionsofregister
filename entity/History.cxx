#include "History.hxx"

History::History()
{
}

History::History(std::string number)
{
  setHistoryNumber(number);
}

History::~History()
{
  for(auto &sanction : sanctions)
    {
      delete sanction;
    }
}

void History::setDepartment(std::string newDepartment)
{
  department = newDepartment;
}

std::string History::getDepartment()
{
  return department;
}

void History::setHistoryNumber(std::string newNumber)
{
  number = newNumber;
}

std::string History::getHistoryNumber()
{
  return number;
}

void History::addSanction(Sanction *sanction)
{
  sanctions.push_back(sanction);
}

const std::vector<Sanction *> History::getSanctions()
{
  return sanctions;
}

std::string History::toString()
{
  std::stringstream ss;
  if (sanctions.empty())
    {
      ss << "\"" << number << "\";\"" << department << "\";\"\";\"\";\"\"" << std::endl;
    }
  else
    {
      for (auto it = sanctions.begin(); it != sanctions.end(); ++it)
	{
	  ss << "\"" << number << "\";\"" << department << "\";\""
	     << (*it)->getCode() << "\";\"" << (*it)->getComment() << "\";\""
	     << (*it)->getAmount().toString() << "\"" << std::endl;
	}
    }

  return ss.str();
}
