#include "Sanction.hxx"

Sanction::Sanction()
{
}

Sanction::Sanction(Money newAmount, unsigned newCode, std::string newComment)
{
  amount = newAmount;
  code = newCode;
  comment = newComment;
}

Sanction::~Sanction()
{
}

Money Sanction::getAmount()
{
  return amount;
}

void Sanction::setAmount(Money newAmount)
{
  amount = newAmount;
}

unsigned Sanction::getCode()
{
  return code;
}

void Sanction::setCode(unsigned newCode)
{
  code = newCode;
}

std::string Sanction::getComment()
{
  return comment;
}

void Sanction::setComment(std::string newComment)
{
  comment = newComment;
}
