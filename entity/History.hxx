#pragma once

#include <string>
#include <vector>
#include <sstream>

#include "Sanction.hxx"

class History
{
public:
  History();
  History(std::string);
  ~History();
  void setHistoryNumber(std::string);
  std::string getHistoryNumber();
  void setDepartment(std::string);
  std::string getDepartment();
  void addSanction(Sanction *);
  const std::vector<Sanction *> getSanctions();
  std::string toString();
private:
  std::string number;
  std::string department;
  std::vector<Sanction *> sanctions;
};
