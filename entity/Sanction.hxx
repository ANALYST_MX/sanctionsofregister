#pragma once

#include <string>

#include "Money.hxx"

class Sanction
{
public:
  Sanction();
  Sanction(Money, unsigned, std::string);
  ~Sanction(); 
  Money getAmount();
  void setAmount(Money);
  unsigned getCode();
  void setCode(unsigned);
  std::string getComment();
  void setComment(std::string);
private:
  Money amount;
  unsigned code;
  std::string comment;
};
