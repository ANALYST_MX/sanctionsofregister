#include "Money.hxx"

Money::Money(unsigned long long value, size_t fracLong)
{
  pennies = value;
  setFracLong(fracLong);
}

Money::Money(const std::string strValue, size_t fracLong)
{
  setFracLong(fracLong);
  if (strValue.empty())
    {
      pennies = 0;
      return;
    }
  std::string::size_type idx;
  double value = std::stod(strValue, &idx);
  pennies = value * frac;
}

Money::Money(const Money &other)
{
  pennies = other.pennies;
  frac = other.frac;
}

Money::~Money()
{
}

std::string Money::toString()
{
  std::ostringstream os;
  os << std::fixed << std::setprecision(getFracLong())
     << 1. * pennies / frac;
  return os.str();
}

void Money::setFracLong(size_t fracLong)
{
  frac = pow(10, fracLong);
}

size_t Money::getFracLong()
{
  return log10(frac);
}

Money &Money::operator = (const Money &rvalue)
{
  pennies = rvalue.pennies;
  frac = rvalue.frac;
  return *this;
}

Money &Money::operator += (const Money &rvalue)
{
  if (frac >= rvalue.frac)
    {
      pennies += rvalue.pennies * (frac / rvalue.frac);
    }
  else
    {
      pennies = pennies * (rvalue.frac / frac) + rvalue.pennies;
      frac = rvalue.frac;
    }
  return *this;
}

Money &Money::operator -= (const Money &rvalue)
{
  if (frac >= rvalue.frac)
    {
      pennies -= rvalue.pennies * (frac / rvalue.frac);
    }
  else
    {
      pennies = pennies * (rvalue.frac / frac) - rvalue.pennies;
      frac = rvalue.frac;
    }
  return *this;
}

Money Money::operator + (const Money &other)
{
  decltype(pennies) tPennies = pennies;
  decltype(frac) tFrac = frac;
  if (tFrac >= other.frac)
    {
      tPennies += other.pennies * (tFrac / other.frac);
    }
  else
    {
      tPennies = tPennies * (other.frac / tFrac) + other.pennies;
      tFrac = other.frac;
    }
  Money money(tPennies, tFrac);
  return money;
}

Money Money::operator - (const Money &other)
{
  decltype(pennies) tPennies = pennies;
  decltype(frac) tFrac = frac;
  if (tFrac >= other.frac)
    {
      tPennies -= other.pennies * (tFrac / other.frac);
    }
  else
    {
      tPennies = tPennies * (other.frac / tFrac) - other.pennies;
      tFrac = other.frac;
    }
  Money money(tPennies, tFrac);
  return money;
}

bool Money::operator == (const Money &other)
{
  if (frac >= other.frac)
    {
      return pennies == other.pennies * (frac / other.frac);
    }
  else
    {
      return pennies * (other.frac / frac) == other.pennies;
    }
}

bool Money::operator != (const Money &other)
{
  if (frac >= other.frac)
    {
      return pennies != other.pennies * (frac / other.frac);
    }
  else
    {
      return pennies * (other.frac / frac) != other.pennies;
    }
}

bool Money::operator > (const Money &other)
{
  if (frac >= other.frac)
    {
      return pennies > other.pennies * (frac / other.frac);
    }
  else
    {
      return pennies * (other.frac / frac) > other.pennies;
    }
}

bool Money::operator < (const Money &other)
{
  if (frac >= other.frac)
    {
      return pennies < other.pennies * (frac / other.frac);
    }
  else
    {
      return pennies * (other.frac / frac) < other.pennies;
    }
}

bool Money::operator >= (const Money &other)
{
  if (frac >= other.frac)
    {
      return pennies >= other.pennies * (frac / other.frac);
    }
  else
    {
      return pennies * (other.frac / frac) >= other.pennies;
    }
}

bool Money::operator <= (const Money &other)
{
  if (frac >= other.frac)
    {
      return pennies <= other.pennies * (frac / other.frac);
    }
  else
    {
      return pennies * (other.frac / frac) <= other.pennies;
    }
}
