#pragma once

#include <string>
#include <sstream>
#include <cmath>
#include <iomanip>

class Money
{
public:
  Money(unsigned long long = 0, size_t = 2);
  Money(const std::string, size_t = 2);
  Money(const Money &);
  ~Money();
  size_t getFracLong();
  Money operator + (const Money &);
  Money operator - (const Money &);
  Money &operator = (const Money &);
  Money &operator += (const Money &);
  Money &operator -= (const Money &);
  bool operator == (const Money &);
  bool operator != (const Money &);
  bool operator > (const Money &);
  bool operator < (const Money &);
  bool operator >= (const Money &);
  bool operator <= (const Money &);
  std::string toString();
protected:
  void setFracLong(size_t);
public:
  unsigned long long pennies;
  size_t frac;
};
